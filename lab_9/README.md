###Mandatory
1. Session: Login & Logout
    1. [ ] Implementasi fungsi Login
        Jika belum login akan ditampilkan login.html (cek dengan request.session),
        login.html menggunakan lab-9:auth_login pada custom_auth.py,
        auth_login mengambil username dan password yang diniput dan verify_user dengan csui_helper.py,
        response = requests.get(API_VERIFY_USER, params=parameters) -> access_token, client_id
        response dibalikkan verify_user, masukan ke request.session
        akan dicek jika user_login sudah di request.session maka langsung menampilkan profile,
        jika tidak akan minta user untuk login
    2. [ ] Implementasi fungsi Logout
        mengunggkan lab-9:auth_logout pada custom_auth.py
        menghapus semua session request.session.flush()
        kembali ke lab-9:index

2. Session: Kelola Favorit
    1. [ ] Implementasi fungsi "Favoritkan" untuk Drones
        pada drones.html button ke add_session_drones di views.py
        mengecek apakah 'drones' sudah ada di request.session atau belum,
        jika belum masukan id nya ke request.session['drones'],
        jika sudah ada yang difavorit sebelumnya dan id belum di tambahkan ke favorit,
        mengembalikan id sebelumnya, append id baru, simpan kembali ke request.session['drones']
        lanjut ke lab-9:profil dan ke set_data_for_session untuk semua data, saat tampilin di drones.html
        jika sudah ada di fav gunakan button hapus, jika belum button Tambahkan
    2. [ ] Implementasi fungsi "Hapus dari favorit" untuk Drones
        del_session_drones pada views.py dengan menampilkan id-id dengan request.session['drones']
        remove id yang ingin dihapus, masukan kembali ke request.session['drones'] kembali ke profile
    3. [ ] Implementasi fungsi "Reset favorit" untuk Drones
        clear_session_drones dengan del request.session['drones'] kembali ke Profile

3. Cookies: Login & Logout
    1. [ ] Implementasi fungsi Login
        cookie_auth_login cek dengan my_cookie_auth (Benar atau salah)
        jika benar cek apabila sudah login (ada di request.COOKIES) tampilkan Profile,
        set cookie -> res.set_cookie('key', value)
    2. [ ] Implementasi fungsi Logout
        delete cookie -> res.delete_cookie('key')

  4. Implementasi Header dan Footer
      1. [ ] Buatlah header yang berisi tombol Logout *hanya jika* sudah login
      (baik pada session dan cookies). Buatlah sebagus dan semenarik mungkin.
      dengan JavaScript -> tombol logout hanya ada pada Profile, pada login.html di hide

### Challenge
1. Implementasi API Optical dan SoundCard
    1. [ ] Menambahkan link ke tab Optical dan Soundcard
      tambah optical.html dan soundcard.html, sesuaikan profile.html pada folder session (url nya dan id)
    2. [ ] Membuat tabel berisi data optical/soundcard
      pada halaman Session Profile get dari api,
      tambah response get_soundcard dan get_optical,
      render untuk ditampilkan ke html, loop untuk menampilkan
2. Implementasi fungsi umum yang sudah disediakan mengelola session:
    1. [ ] Menggunakan fungsi umum untuk menambahkan (Favoritkan) optical/soundcard ke session
      menyesuakan pemanggilan views dari url di html:parameter = category_description dan id,
      langkah Selanjutnya sama seperti drones dengan cocokan key yaitu category_description dengan di request.session
    2. [ ] Menggunakan fungsi umum untuk menghapus (Hapus dari Favorit) optical/soundcard dari session
      sama seperti drone tapi dengan request.session[key] dengan key nya category_description
    3. [ ] Menggunakan fungsi umum untuk menghapus/reset kategori (drones/optical/soundcard) dari session.
      del request.session[key]
