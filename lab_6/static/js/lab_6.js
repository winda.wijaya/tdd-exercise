//Chat box

//appendChat
function appendChat(e) {
    var notEmpty = $(".usertext").val() != "";
    var rply = ["<b>admin: </b> Iya",
    						"<b>admin: </b> Tidak",
    						"<b>admin: </b> Nggak",
    						"<b>admin: </b> Bisa jadi",
    						"<b>admin: </b> Menarik",
    						"<b>admin: </b> Gak tau",
    						]

    if(e.which == 13) {
		var txt = "<b>me: </b>"+$(".usertext").val();
		var min = 0;
		var max = 5;
		var random = Math.floor(Math.random() * (max - min + 1)) + min;
    if(notEmpty){
  		$('<p>'+txt+'</p>').addClass('msg-send').appendTo('.msg-insert');
  		$(".usertext").val("");
  		$('<p>'+rply[random]+'</p>').addClass('msg-receive').appendTo('.msg-insert');
  		$(".chat-body").scrollTop($(".chat-body").height());
    }else{
      alert("Message Empty");
      }
    }
}

//reset textarea
function reset(e){
  if (e.which == 13) {
      $(".msg-text-area").val("");
  }
}
$(".msg-text-area").keypress(appendChat);

$(".msg-text-area").keyup(reset);
//END Chat Box

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
	if (x === 'ac') {
    print.value = "";
	}
	else if (x === 'eval') {
		print.value = Math.round(evil(print.value) * 10000) / 10000;
		erase = true;
	}else if(x === 'log'){
    print.value = Math.log10(Math.round(evil(print.value) * 10000) / 10000).toFixed(12);
    erase = true;
  }else if(x === 'sin'){
    print.value = Math.sin(Math.round(evil(print.value) * 10000) / 10000).toFixed(12);
    erase = true;
  }else if(x === 'tan'){
    print.value = Math.tan(Math.round(evil(print.value) * 10000) / 10000).toFixed(12);
    erase = true;
  }
	else {
		print.value += x;
	}
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END Calculator

//Change themes

$(document).ready(function () {
    var themes = [{"id": 0, "text": "Red", "bcgColor": "#F44336", "fontColor": "#FAFAFA"},
    {"id": 1, "text": "Pink", "bcgColor": "#E91E63", "fontColor": "#FAFAFA"},
    {"id": 2, "text": "Purple", "bcgColor": "#9C27B0", "fontColor": "#FAFAFA"},
    {"id": 3, "text": "Indigo", "bcgColor": "#3F51B5", "fontColor": "#FAFAFA"},
    {"id": 4, "text": "Blue", "bcgColor": "#2196F3", "fontColor": "#212121"},
    {"id": 5, "text": "Teal", "bcgColor": "#009688", "fontColor": "#212121"},
    {"id": 6, "text": "Lime", "bcgColor": "#CDDC39", "fontColor": "#212121"},
    {"id": 7, "text": "Yellow", "bcgColor": "#FFEB3B", "fontColor": "#212121"},
    {"id": 8, "text": "Amber", "bcgColor": "#FFC107", "fontColor": "#212121"},
    {"id": 9, "text": "Orange", "bcgColor": "#FF5722", "fontColor": "#212121"},
    {"id": 10, "text": "Brown", "bcgColor": "#795548", "fontColor": "#FAFAFA"}];

    localStorage.setItem("themes", JSON.stringify(themes));

    mySelect = $('.my-select').select2();

    mySelect.select2({
        'data': JSON.parse(localStorage.getItem("themes")
        )
    });

    //ambil dan parsing data 'themes' dari localStorage
    var arraysOfTheme = JSON.parse(localStorage.getItem("themes"));
    var selectedTheme = arraysOfTheme[3];


    if (localStorage.getItem("selectedTheme") !== null) {
        selectedTheme = JSON.parse(localStorage.getItem("selectedTheme"));
    }


    //ganti css sesuai theme yang dipilih (selectedTheme)
    $('body').css(
        {
            "background-color": selectedTheme.bcgColor,  "font-color": selectedTheme.fontColor
        }
    );

    //terapkan tema apabila tombol 'apply' ditekan
    $('.apply-button').on('click', function () {
        //id theme yang dipilih
        var idThemeChoosed = mySelect.val();

        //set selectedTheme
        selectedTheme = arraysOfTheme[idThemeChoosed]

        //ganti css sesuai theme yang dipilih
        $('body').css(
            {
                "background-color": selectedTheme.bcgColor,
                "font-color": selectedTheme.fontColor
            }
        );

        //simpan 'selectedTheme' di localStorage
        localStorage.setItem("selectedTheme", JSON.stringify(selectedTheme));
    })
});


//END Change themes
