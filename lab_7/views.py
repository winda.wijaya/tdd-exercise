from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.forms.models import model_to_dict

from .models import Friend, Mahasiswa
from api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada
    list_mahasiswa = csui_helper.instance.get_mahasiswa_list()

    for mahasiswa in list_mahasiswa:
        mahasiswaX = Mahasiswa(mahasiswa_name=mahasiswa['nama'], npm=mahasiswa['npm'])
        mahasiswaX.save()

    paginator = Paginator(list_mahasiswa, 25)
    mahasiwa_list = Mahasiswa.objects.all()
    page = request.GET.get('page')
    try:
        halaman = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        halaman = paginator.page(1)


    friend_list = Friend.objects.all()
    response = {"mahasiswa_list": halaman, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def friend_list_json(request): # update
    friends = [obj.as_dict() for obj in Friend.objects.all()]
    return JsonResponse({"results": friends})

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        valid = isNpmValid(npm)
        data = {'validity' : valid}
        if data['validity'] == True:
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
            data['friends'] = model_to_dict(friend)
        return JsonResponse(data)

@csrf_exempt
def delete_friend(request):
    if request.method == 'POST':
        friend_id = request.POST['id']
        Friend.objects.filter(id=friend_id).delete()
        response = {'id' : friend_id}
        return JsonResponse(response)

@csrf_exempt
def validate_npm(npm):
    data = {'is_taken': not isNpmValid(npm)}
    return JsonResponse(data)

def isNpmValid(npm):
    friends = Friend.objects.all()
    for friend in friends:
        if friend.npm == npm:
            return False
    return True

# def model_to_dict(obj):
#     data = serializers.serialize('json', [obj,])
#     struct = json.loads(data)
#     data = json.dumps(struct[0]["fields"])
#     return data
