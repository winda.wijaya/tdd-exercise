// FB initiation function
window.fbAsyncInit = function() {
  FB.init({
    appId      : '214831122392536',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
          render(true);
      } else if (response.status === 'not_authorized') {
          render(false);
      } else {
          render(false);
      }
  });

  // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
  // dan jalankanlah fungsi render dibawah, dengan parameter true jika
  // status login terkoneksi (connected)

  // Hal ini dilakukan agar ketika web dibuka, dan ternyata sudah login, maka secara
  // otomatis akan ditampilkan view sudah login
};

// Call init facebook. default dari facebook
(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s);
   js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));

// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Rubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
const render = loginFlag => {
  if (loginFlag) {
    document.getElementById("loginLogout").innerHTML="LOGOUT";
    // Jika yang akan dirender adalah tampilan sudah login

    // Panggil Method getUserData yang anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
    getUserData(user => {
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('#cover').html(
        '<div class="profile">' +
          '<img style="width:100%"class="cover" src="' + user.cover.source + '" alt="cover" />' +
          '<img style="border-radius:50%" class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
          '<div class="data">' +
            '<h1>' + user.name + '</h1>' +
            '<h3>' + user.email + ' - ' + 'Female' + '</h3>' +
          '</div>' +
        '</div>' +
        '<div><input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" /></div>' +
        '<div><button class="btn btn-lg btn-success postStatus" onclick="postStatus()">Post ke Facebook</button></div>' +
        '<button class="btn btn-lg btn-warning logout" onclick="facebookLogout()">Logout</button>'
      );

      // Setelah merender tampilan diatas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserFeed(feed => {
        feed.data.map(value => {
          // Render feed, kustomisasi sesuai kebutuhan.
          var time = new Date(value.created_time);
          if (value.message && value.story) {
            $('#lab8').append(
              '<div class="flex-item"> <div class="gradient">' +
                '<h1 class="title">' + value.message + '</h1>' +
                '<h2 class="content">' + value.story + '</h2>' +
                '<p>' + time + '</p>' +
                '<button class="btn btn-lg btn-danger delete" onclick="deleteStatus('+ value.id +')">Delete</button>'+
              '</div></div>'
            );
          } else if (value.message) {
            $('#lab8').append(
              '<div class="flex-item"> <div class="gradient">' +
                '<h1 class="title">' + value.message + '</h1>' +
                '<p>' + time + '</p>' +
                '<button class="btn btn-lg btn-danger delete" onclick="deleteStatus('+ value.id +')">Delete</button>'+
              '</div> </div>'
            );
          } else if (value.story) {
            $('#lab8').append(
              '<div class="flex-item"> <div class="gradient">' +
                '<h2 class="content">' + value.story + '</h2>' +
                '<p>' + time + '</p>' +
                '<button class="btn btn-lg btn-danger delete" onclick="deleteStatus('+ value.id +')">Delete</button>'+
              '</div> </div>'
            );
          }
        });
      });
    });
  // } else {
  //   // Tampilan ketika belum login
  //   $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
  }
};

const facebookLogin = () => {
  document.getElementById("loginLogout").innerHTML="LOGOUT";
  console.log(response);
  FB.login(function(response){
    response.status ==='connected' ? render(true) : render(false);
    render(true);
  }, {scope:'public_profile,user_posts,publish_actions,user_about_me,email'})
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
  // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
  // pada scope yang ada.
};

const facebookLogout = () => {
  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      FB.logout();
      render(false);
    }
  });
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
  // ketika logout sukses.
};

const getUserData = (fun) => {
  FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.api('/me?fields=id,name,cover,about,email,picture', 'GET', response => {
          fun(response)
        });
      }
  });
  // TODO: Implement Method Ini
  // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data User dari akun
  // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
  // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
  // tersebut
};

const getUserFeed = (fun) => {
  FB.api('/me/feed', 'GET', response => {
    fun(response);
  })

  // TODO: Implement Method Ini
  // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
  // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
  // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
  // tersebut
};

const postFeed = message => {
  // var message = "Hello World!";
  FB.api('/me/feed', 'POST', {message:message});
  render(true);
  // Todo: Implement method ini,
  // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
  // Melalui API Facebook dengan message yang diterima dari parameter.
};

const postStatus = () => {
  const message = $('#postInput').val();
  postFeed(message);
};

const deleteStatus = id => {
    console.log(id);
    //var idToDel = "/me/" + id;
    FB.api('/'+id, 'delete', response => {
        if (response && !response.error) {
            // $("#" + id).remove();
        } else {
            alert("Something went wrong");
        }
    });
};
